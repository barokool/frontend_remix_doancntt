import {
  useLoaderData,
  useLocation,
  useNavigate,
  useTransition,
} from "@remix-run/react";
import React, { useEffect, useState } from "react";
import HomeLayout from "~/layouts/home-layout/HomeLayout";

import BreadCrumb from "~/components/BreadCrumb";
import {
  Box,
  Button,
  Grid,
  GridItem,
  HStack,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from "@chakra-ui/react";
import { Image } from "@chakra-ui/react";
import styled from "@emotion/styled";
import { removeSpaceString } from "~/utils/functions/removeSpace";
import BaseButton from "~/components/Button";
import TabProductDetail from "~/components/TabProductDetail";
import type { IProduct } from "~/models/products";
import { getProductBySlug } from "~/models/products";
import type { LoaderFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { useRouteData } from "~/hooks/useRouteData";
import { isObjectNull } from "~/utils/functions/object";
const ProductDetail = () => {
  const { pathname } = useLocation();
  const path = pathname.split("/")?.[1].split("-");

  const data = useLoaderData();
  const product: IProduct = data && data[0];

  return (
    <HomeLayout>
      <BreadCrumb path={path} css={{ marginTop: 20 }} label={breadCrum.label} />
      <Grid
        templateAreas={`"nav img main"
                  "nav img main"
                  "nav img main"`}
        gridTemplateColumns={"150px 1fr 1fr"}
        gap="1"
        color="blackAlpha.700"
        fontWeight="bold"
        marginTop={"50px"}
        maxH={"700px"}
      >
        <GridItem maxH={"700px"} area={"nav"}>
          <div
            style={{
              maxHeight: "700px",
              height: "100%",
              overflow: "auto",
              display: "flex",
              gap: "16px",
              flexDirection: "column",
            }}
          >
            {images &&
              images.map((item, index) => {
                return (
                  <Box key={index} width={"100%"}>
                    <Image
                      objectFit={"cover"}
                      src={item}
                      alt={`${index + Math.random()}`}
                    />
                  </Box>
                );
              })}
          </div>
        </GridItem>
        <GridItem area={"img"}>
          <Box width={"100%"} h={"100%"} maxH={"700px"}>
            <Image
              objectFit="cover"
              h={"100%"}
              src={product.images?.[0]}
              alt="Dan Abramov"
            />
          </Box>
        </GridItem>

        <GridItem area={"main"}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              flexDirection: "column",
              height: "100%",
              maxHeight: "700px",
            }}
          >
            <ProductDetailComponent product={product} />
            <DoubleButtons product={product} />
          </div>
        </GridItem>
      </Grid>
      <div style={{ marginTop: "40px" }}>
        <TabProductDetail />
      </div>
    </HomeLayout>
  );
};

export default ProductDetail;

export const loader: LoaderFunction = async ({ request, params }) => {
  const slug = params.slug;
  if (!slug) return;
  const product = await getProductBySlug(slug);

  return json(product.data);
};

interface IProductDetailComponentProps {
  product: IProduct;
}

const ProductDetailComponent: React.FC<IProductDetailComponentProps> = ({
  product,
}) => {
  return (
    <ProductWrapper>
      <ProductTitle>{product.name}</ProductTitle>
      <ProductPrice>
        Price :{" "}
        <span style={{ fontWeight: "800" }}>
          {removeSpaceString(product.price)} $
        </span>
      </ProductPrice>
      <SizeContainer>
        <p>Size </p>
        <ButtonWrapper>
          {sizeProduct.map((item, index) => {
            return <ButtonContainer key={index}>{item}</ButtonContainer>;
          })}
        </ButtonWrapper>
      </SizeContainer>

      <div>
        <h3>
          Author : <span style={{ fontWeight: "800" }}>Baro gia</span>
        </h3>
      </div>
      <NoteContainer>
        <h2
          style={{
            fontSize: "24px",
            fontWeight: "600",
            color: "black",
          }}
        >
          Online Only
        </h2>
        <p>
          This product is exclusive to online shopping and may only be purchased
          and returned online as it is not stocked in-store.
        </p>
      </NoteContainer>
    </ProductWrapper>
  );
};

interface IDoubleButtonsProps {
  product: IProduct;
}
const DoubleButtons: React.FC<IDoubleButtonsProps> = ({ product }) => {
  const routeData = useRouteData("root");
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const user = routeData?.data.user || {};
  const { isOpen, onOpen, onClose } = useDisclosure();
  let cart: IProduct[] = [];
  const [addSuccess, setAddSuccess] = useState(false);

  const handleAddToCart = () => {
    onOpen();
    if (!isObjectNull(user)) {
      cart = JSON.parse(localStorage.getItem("cart")!);

      if (!cart) {
        cart = [product];
        setAddSuccess(true);
      } else {
        const isExisted = cart.some((item) => item._id === product._id);
        if (isExisted) setAddSuccess(false);
        if (!isExisted) {
          cart.push(product);
          setAddSuccess(true);
        }
      }

      localStorage.setItem("cart", JSON.stringify(cart));
    }
  };

  const renderModel = () => {
    if (isObjectNull(user)) {
      return (
        <Modal onClose={onClose} isOpen={isOpen} isCentered>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Login Issue</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <p>
                You haven't logined yet. Please login to add the product into
                your cart! Sorry for this
              </p>
            </ModalBody>
            <ModalFooter>
              <HStack>
                <Button fontWeight={700} onClick={onClose}>
                  Close
                </Button>
                <Button
                  fontWeight={700}
                  onClick={() => navigate(`/login?redirect=${pathname}`)}
                >
                  Login
                </Button>
              </HStack>
            </ModalFooter>
          </ModalContent>
        </Modal>
      );
    }
    if (!isObjectNull(user) && !addSuccess) {
      return (
        <Modal onClose={onClose} isOpen={isOpen} isCentered>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Cart</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <p>This product has already in your cart! Have a nice day!</p>
            </ModalBody>
            <ModalFooter>
              <HStack>
                <Button fontWeight={700} onClick={onClose}>
                  Close
                </Button>
                <Button fontWeight={700} onClick={() => navigate(`/cart`)}>
                  Go to Cart
                </Button>
              </HStack>
            </ModalFooter>
          </ModalContent>
        </Modal>
      );
    }
    if (!isObjectNull(user) && addSuccess) {
      return (
        <Modal onClose={onClose} isOpen={isOpen} isCentered>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Cart</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <p>
                Your product has been added success into your cart! Wish you
                have a nice day!
              </p>
            </ModalBody>
            <ModalFooter>
              <HStack>
                <Button fontWeight={700} onClick={onClose}>
                  Close
                </Button>
                <Button fontWeight={700} onClick={() => navigate(`/cart`)}>
                  Go to Cart
                </Button>
              </HStack>
            </ModalFooter>
          </ModalContent>
        </Modal>
      );
    }
  };
  return (
    <ButtonsShipping>
      <BaseButton
        style={{
          backgroundColor: "rgba(51, 204, 204, 1) ",
          outline: "none",
          width: "100%",
          color: "white",
          border: "none",
          fontWeight: "700",
        }}
        title="Add to cart"
        onClick={() => handleAddToCart()}
      />
      {renderModel()}
      <BaseButton
        style={{
          backgroundColor: "rgba(51, 204, 204, 1) ",
          outline: "none",
          width: "100%",
          color: "white",
          border: "none",
          padding: "10px 20px",
          fontWeight: "700",
        }}
        title="Buy now"
      />
    </ButtonsShipping>
  );
};

const breadCrum = {
  path: "/product-detail",
  label: "Product",
};

const images = [
  "https://demofree.sirv.com/nope-not-here.jpg",
  "https://demofree.sirv.com/nope-not-here.jpg",
  "https://demofree.sirv.com/nope-not-here.jpg",
  "https://demofree.sirv.com/nope-not-here.jpg",
];

const sizeProduct = ["Size S", "Size M", "Size L", "Size XL", "Size 2XL"];
const ProductWrapper = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  gap: 20px;
`;
const ProductTitle = styled.h1`
  font-size: 30px;
  color: black;
`;

const ProductPrice = styled.p`
  font-weight: 700;
  font-size: 26px;
`;

const SizeContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
`;
const ButtonWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 10px;
`;

const ButtonContainer = styled.button`
  font-size: 14px;
  border-width: 1px;
  font-weight: 500;
  line-height: 1;
  cursor: pointer;
  border-radius: 5px;
  padding: 5px 10px;

  &:hover {
    background-color: rgba(248, 248, 248, 1);
  }
  &:focus {
    background-color: rgba(51, 204, 204, 1);
    border: none;
    color: white;
  }
`;

const ButtonsShipping = styled.div`
  display: flex;
  gap: 20px;
  justify-content: space-between;
`;

const NoteContainer = styled.div`
  border: 2px solid var(--primary);
  border-radius: 4px;
  padding: 10px 20px;
`;
