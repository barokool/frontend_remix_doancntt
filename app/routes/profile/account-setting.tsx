import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import type { ActionArgs } from "@remix-run/node";
import { Form, useActionData } from "@remix-run/react";
import React from "react";
import { FormProvider, useForm } from "react-hook-form";
import BaseInput from "~/components/Input";
import { styled } from "~/design-system/stitches.config";
import LockProfile from "~/icons/Lock.profile";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { updatePassword } from "~/models/auth";
import { getUserToken } from "~/utils/auth";
import { useEffect } from "react";

export const action = async ({ request }: ActionArgs) => {
  const token = await getUserToken(request);
  if (!token) return null;
  const form = await request.formData();
  const password = form.get("password") as string;
  const newPassword = form.get("newPassword") as string;
  try {
    const data = await updatePassword(
      { password, newPassword },
      token as string
    );
    console.log("data action : ", data);

    return data;
  } catch (error) {
    console.log(error);
    return error;
  }
};

const AccountSetting = () => {
  const { onOpen, onClose, isOpen } = useDisclosure();
  const data = useActionData();
  const toast = useToast();
  useEffect(() => {
    if (data) {
      toast({
        title: "Updated password success",
        status: "success",
        duration: 2000,
        isClosable: true,
        position: "top-right",
      });
    }
  }, [data]);

  return (
    <Wrap>
      <p>Security</p>
      <Flex css={{ justifyContent: "space-between", marginTop: "$x5" }}>
        <Flex>
          <LockProfile />
          <ContentWrap>
            <h2>Change your password</h2>
            <p>
              It's a good idea to use a strong password that you're not using
              elsewhere
            </p>
          </ContentWrap>
        </Flex>
        <Button onClick={onOpen}>Edit</Button>
      </Flex>
      <ModelChangePass isOpen={isOpen} onClose={onClose} />
    </Wrap>
  );
};

export default AccountSetting;

type ModelProps = {
  isOpen: boolean;
  onClose: () => void;
};

interface IFormValue {
  password: string;
  newPassword: string;
  confirmPassword: string;
}

const schema = yup
  .object({
    password: yup
      .string()
      .required("This field is required")
      .min(6, "More than 6 characters"),
    newPassword: yup
      .string()
      .required("This field is required")
      .min(6, "More than 6 characters"),
    confirmPassword: yup
      .string()
      .required("This field is required")
      .min(6, "More than 6 characters")
      .oneOf(
        [yup.ref("newPassword"), null],
        "This is not match with the new password"
      ),
  })
  .required();

const ModelChangePass = ({ isOpen, onClose }: ModelProps) => {
  const formMethods = useForm<IFormValue>({
    defaultValues: {
      password: "",
      newPassword: "",
      confirmPassword: "",
    },
    mode: "onBlur",
    resolver: yupResolver(schema),
  });

  const {
    register,
    trigger,
    formState: { errors },
  } = formMethods;

  const onHandleValidate = async (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    const wrongValidate = await trigger([
      "password",
      "newPassword",
      "confirmPassword",
    ]);
    if (!wrongValidate) e.preventDefault();
    else onClose();
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <FormProvider {...formMethods}>
        <ModalContent>
          <Form method="post">
            <ModalHeader>Change password</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <Flex css={{ display: "grid" }}>
                <Field>
                  <Label>Current password</Label>
                  <InputField
                    type="password"
                    placeholder="Your current password"
                    {...register("password")}
                  />
                  {errors.password && <Span>{errors.password.message}</Span>}
                </Field>
                <Field>
                  <Label>New password</Label>
                  <InputField
                    type="password"
                    placeholder="Your new password"
                    {...register("newPassword")}
                  />
                  {errors.newPassword && (
                    <Span>{errors.newPassword.message}</Span>
                  )}
                </Field>

                <Field>
                  <Label>Confirm password</Label>
                  <InputField
                    type="password"
                    placeholder="Your confirm password"
                    {...register("confirmPassword")}
                  />
                  {errors.confirmPassword && (
                    <Span>{errors.confirmPassword.message}</Span>
                  )}
                </Field>
              </Flex>
            </ModalBody>

            <ModalFooter>
              <Flex>
                <Button type="button" notPrimary={true} onClick={onClose}>
                  Close
                </Button>
                <Button onClick={onHandleValidate} type="submit">
                  Change
                </Button>
              </Flex>
            </ModalFooter>
          </Form>
        </ModalContent>
      </FormProvider>
    </Modal>
  );
};

const Wrap = styled("div", {
  "> *:first-child": {
    boldHeader: "$22px",
  },
});

const Flex = styled("div", {
  display: "flex",
  gap: "$x4",
  alignItems: "center",
});

const ContentWrap = styled("div", {
  h2: {
    mediumBody: "$16px",
  },
  p: {
    regularBody: "$14px",
  },
});

const Button = styled("button", {
  buttonStyle: "primary",
  padding: "$x1 $x4",
  borderRadius: "$space$x1",

  variants: {
    notPrimary: {
      true: {
        background: "white",
        color: "black",
        border: "1px solid $neutral500",
        "&:hover, &:focus-visible": {
          background: "$neutral100",
        },
      },
    },
  },
});

const Field = styled("div", {
  display: "flex",
  flexDirection: "column",
});

const Label = styled("label", {
  mediumBody: "$16px",
  marginBottom: "5px",
});

const InputField = styled(BaseInput, {
  borderRadius: "$space$x1",
  border: "1px solid $neutral500",
  padding: "0 15px",
  height: "45px",
  mediumBody: "$14px",
  backgroundColor: "rgba(255, 255, 255, 1)",
  variants: {
    isDisabled: {
      true: {
        backgroundColor: "$neutral100",
      },
    },
  },
});

const Span = styled("span", {
  mediumBody: "$14px",
  color: "$red300",
});
