import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useToast,
} from "@chakra-ui/react";
import type {
  ActionArgs,
  ActionFunction,
  LoaderFunction,
} from "@remix-run/node";
import { json } from "@remix-run/node";
import { Form, useActionData, useLoaderData } from "@remix-run/react";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { FormProvider, useForm, useFormContext } from "react-hook-form";
import BaseButton from "~/components/Button";
import BaseInput from "~/components/Input";
import { styled } from "~/design-system/stitches.config";
import { useRouteData } from "~/hooks/useRouteData";
import type { IUpdateUser, IUser } from "~/models/auth";
import { updateAccount } from "~/models/auth";
import { getUserToken, parseJwt, requireUserValidToken } from "~/utils/auth";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { getUserByEmail } from "~/models/user";
export const loader: LoaderFunction = async ({ request }) => {
  const userToken = await getUserToken(request);

  const data = parseJwt(userToken || "");
  const user = await getUserByEmail(data && data.email);

  return json(user);
};

export const action = async ({ request }: ActionArgs) => {
  let token = await getUserToken(request);
  if (!token) return null;
  const form = await request.formData();
  //form data
  const email = (form.get("email") as string) || "";
  const password = (form.get("password") as string) || "";
  const displayName = (form.get("displayName") as string) || "";
  const address = (form.get("address") as string) || "";
  const phoneNumber = (form.get("phoneNumber") as string) || "";

  let input: IUpdateUser = {};

  if (typeof email === "string" && email !== "") {
    input = { ...input, email };
  }
  if (typeof password === "string" && password !== "")
    input = { ...input, password };

  if (typeof displayName === "string" && displayName !== "")
    input = { ...input, displayName };

  if (typeof address === "string" && address !== "")
    input = { ...input, address };

  if (typeof phoneNumber === "string" && phoneNumber !== "")
    input = { ...input, phoneNumber };

  try {
    const data = await updateAccount(input, token);
    return json(data);
  } catch (error) {
    console.log("errorrrr : ", error);
    return error;
  }

  // const data = await updateAccount()
};

interface IFormValue {
  email: string;
  password: string;
  displayName: string;
  address: string;
  phoneNumber: string;
}

const schema = yup
  .object({
    email: yup.string().optional().email("Email is invalid"),
    password: yup
      .string()
      .required("This field is required")
      .min(6, "Please enter more than 6 characters"),
    displayName: yup.string().optional(),
    address: yup.string().optional(),
    phoneNumber: yup.string().optional(),
  })
  .required();

const ProfileSetting = () => {
  const { user } = useLoaderData<{ user: IUser }>();
  const toast = useToast();

  const formMethods = useForm<IFormValue>({
    defaultValues: {
      email: user.email,
      address: user.address,
      displayName: user.displayName,
      phoneNumber: user.phoneNumber,
      password: "",
    },
    mode: "onBlur",
    resolver: yupResolver(schema),
  });

  //state for disable input field
  const [disabledField, setDisabledField] = useState(true);

  const {
    register,
    setError,
    formState: { errors },
    watch,
  } = formMethods;

  const onToggleEdit = useCallback(() => {
    setDisabledField(!disabledField);
  }, [disabledField]);

  const actionData = useActionData();
  console.log("msg error : ", actionData);
  useEffect(() => {
    if (actionData?.statusCode === 400)
      toast({
        title: "Updated failure.",
        description: "Wrong password.",
        status: "error",
        duration: 2000,
        isClosable: true,
        position: "top-right",
      });
    else if (actionData?.status === 200) {
      toast({
        title: "Updated account.",
        description: "We've updated your account for you.",
        status: "success",
        duration: 2000,
        isClosable: true,
        position: "top-right",
      });
    }
  }, [actionData, setError]);

  return (
    <FormProvider {...formMethods}>
      <Form id="change-yourself" method="post">
        <DoubleButtons onEdit={onToggleEdit} isEdit={!disabledField} />
        <BoxFrame isError={!!errors.password?.message}>
          {errors.password && <Span>{errors.password.message}</Span>}
          <Grid2Col>
            <Field>
              <Label>Email address</Label>
              <InputField
                type="email"
                defaultValue={user.email || "No information"}
                disabled={disabledField}
                isDisabled={disabledField}
                {...register("email")}
              />
              {errors.email && <Span>{errors.email.message}</Span>}
            </Field>
            <Field>
              <Label>Phone number</Label>
              <InputField
                type="text"
                disabled={disabledField}
                defaultValue={user.phoneNumber || "No information"}
                isDisabled={disabledField}
                {...register("phoneNumber")}
              />
              {errors.phoneNumber && <Span>{errors.phoneNumber.message}</Span>}
            </Field>
          </Grid2Col>
          <Field>
            <Label>Your full name</Label>
            <InputField
              type="text"
              defaultValue={user.displayName || "No information"}
              disabled={disabledField}
              isDisabled={disabledField}
              {...register("displayName")}
            />
            {errors.displayName && <Span>{errors.displayName.message}</Span>}
          </Field>
          <Field>
            <Label>Your address</Label>
            <InputField
              type="text"
              defaultValue={user.address || "No information"}
              disabled={disabledField}
              isDisabled={disabledField}
              {...register("address")}
            />
            {errors.address && <Span>{errors.address.message}</Span>}
          </Field>
        </BoxFrame>
      </Form>
    </FormProvider>
  );
};

export default ProfileSetting;

type DoubleButtonProps = {
  isEdit: boolean;
  onEdit: () => void;
};

const DoubleButtons = ({ isEdit, onEdit }: DoubleButtonProps) => {
  const [saveInfo, setSaveInfo] = useState(false);

  const toggleSave = useCallback(() => {
    setSaveInfo(!saveInfo);
  }, [saveInfo]);

  return (
    <ButtonsShipping>
      <Button type="button" onClick={() => onEdit()}>
        {isEdit ? "Cancel" : "Edit"}
      </Button>
      <Button type="button" disabled={!isEdit} onClick={toggleSave}>
        Save
      </Button>
      <ModelChangePass isOpen={saveInfo} onClose={toggleSave} />
    </ButtonsShipping>
  );
};

type ModelProps = {
  isOpen: boolean;
  onClose: () => void;
};

const ModelChangePass = ({ isOpen, onClose }: ModelProps) => {
  const {
    trigger,
    register,
    formState: { errors },
  } = useFormContext<IFormValue>();
  const onHandleValidate = async (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    const wrongValidate = await trigger(["password"], { shouldFocus: true });
    console.log(wrongValidate);

    if (!wrongValidate) {
      e.preventDefault();
    } else {
      onClose();
    }
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Save new Info</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Flex css={{ display: "grid" }}>
            <Field>
              <Label>Current password</Label>
              <InputField
                type="password"
                {...register("password")}
                placeholder="Your current password"
                form="change-yourself"
              />
            </Field>
            {errors.password && <Span>{errors.password.message}</Span>}
          </Flex>
        </ModalBody>

        <ModalFooter>
          <Flex>
            <Button notPrimary={true} onClick={onClose}>
              Close
            </Button>

            <Button
              form="change-yourself"
              onClick={(e) => onHandleValidate(e)}
              type="submit"
            >
              Change
            </Button>
          </Flex>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

const Flex = styled("div", {
  display: "flex",
  gap: "$x4",
  alignItems: "center",
});

const Button = styled("button", {
  buttonStyle: "primary",
  padding: "$x1 $x4",
  borderRadius: "$space$x1",
  variants: {
    notPrimary: {
      true: {
        background: "white",
        color: "black",
        border: "1px solid $neutral500",
        "&:hover, &:focus-visible": {
          background: "$neutral100",
        },
      },
    },
    isDisabledCSS: {
      true: {
        background: "$neutral100",
        color: "$neutral600",
        cursor: "not-allowed",
        border: "1px solid $neutral500",
      },
    },
  },
});

const BoxFrame = styled("div", {
  borderWidth: "1px",
  borderColor: "$neutral500",
  borderRadius: "4px",
  width: "100%",
  marginTop: "20px",
  paddingBlock: "$x10",
  paddingInline: "$x6",
  display: "grid",
  gap: "$x6",
  variants: {
    isError: {
      true: {
        border: "1px solid $red300",
      },
    },
  },
});

const ButtonsShipping = styled("div", {
  display: "flex",
  gap: "20px",
  justifyContent: "flex-end",
});

const Field = styled("div", {
  display: "flex",
  flexDirection: "column",
});

const Label = styled("label", {
  mediumBody: "$16px",
  marginBottom: "5px",
});

const InputField = styled(BaseInput, {
  borderRadius: "$space$x1",
  border: "1px solid $neutral500",
  padding: "0 15px",
  height: "45px",
  mediumBody: "$14px",
  backgroundColor: "rgba(255, 255, 255, 1)",
  variants: {
    isDisabled: {
      true: {
        backgroundColor: "$neutral100",
      },
    },
    isError: {
      true: {
        border: "1px solid $red300",
      },
    },
  },
});

const Grid2Col = styled("div", {
  display: "grid",
  gridTemplateColumns: "repeat(2,1fr)",
  gap: "$x6",
});

const Span = styled("span", {
  mediumBody: "$14px",
  color: "$red300",
});
