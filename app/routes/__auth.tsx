import styled from "@emotion/styled";
import { Outlet } from "@remix-run/react";
import React from "react";
import LoginIcon from "~/icons/Login";

const AuthRoute = () => {
  return (
    <Wrapper>
      <LeftDiv>
        <ImageContainer>
          <LoginIcon />
        </ImageContainer>
      </LeftDiv>
      <RightDiv>
        <Outlet />
      </RightDiv>
    </Wrapper>
  );
};

export default AuthRoute;

const Wrapper = styled.div`
  grid-template-columns: 1fr 1fr;
  grid-template-areas: "svg form";
  display: grid;
  height: 100vh;
  max-width: 1600px;
  margin: 0 auto;
`;

const LeftDiv = styled.div`
  width: 100%;
  background-color: rgba(246, 246, 251, 1);
`;
const RightDiv = styled.div`
  width: 100%;
`;

const ImageContainer = styled.div`
  max-width: 550px;
  height: 100%;
  margin: 0 auto;
  grid-area: "svg";
  display: flex;
  justify-content: center;
  align-items: center;
`;
