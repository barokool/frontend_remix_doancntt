import styled from "@emotion/styled";
import type { LoaderFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import React from "react";
import Pagination from "~/components/Pagination";
import ProductCard from "~/components/ProductCard";
import { useKeepPosition } from "~/hooks/useKeepPosition";
import type { IProduct } from "~/models/products";
import { getAllProduct } from "~/models/products";

const ProductOutlet = () => {
  const { count, result: products } = useLoaderData<{
    count: number;
    result: IProduct[];
  }>();
  useKeepPosition();
  return (
    <Wrapper>
      {/* <p>Product Outlet</p> */}
      <ProductsWrapper>
        {products &&
          products.map((product, index) => {
            return <ProductCard key={product._id} product={product} />;
          })}
      </ProductsWrapper>
      <Pagination
        css={{ marginTop: "$x6" }}
        itemPerPage={10}
        totalItems={count}
      />
    </Wrapper>
  );
};

export default ProductOutlet;

export const loader: LoaderFunction = async ({ request }) => {
  const searchParams = new URL(request.url).searchParams;

  const page = +(searchParams.get("page") || 1);

  const products = await getAllProduct(page, 10, "");

  return json(products);
};

const Wrapper = styled.div``;

const ProductsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 20px;
  margin-top: 20px;
`;
