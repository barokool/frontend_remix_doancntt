import styled from "@emotion/styled";
import React from "react";
import BaseButton from "~/components/Button";
import BaseInput from "~/components/Input";
import AuthLayout from "~/layouts/auth-layout";

const ForgotPasswordRoute = () => {
  return (
    <AuthLayout
      title="Password Reset"
      description={
        <div>
          <Description>
            Enter your email, and we'll send you a password reset link. This may
            take a few minutes!
          </Description>
        </div>
      }
      Form={
        <Form>
          <Field>
            <Label>Your Email Address</Label>
            <InputField type="text" placeholder="Enter your email" />
          </Field>
          <SubmitButton title="Next" />
        </Form>
      }
    />
  );
};

export default ForgotPasswordRoute;

const Description = styled.h3`
  font-size: 16px;
  font-weight: 400;
  color: rgba(100, 100, 100, 1);
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  gap: 20px;
  margin: 20px 0;
`;

const Field = styled.div`
  display: flex;
  flex-direction: column;
`;

const Label = styled.label`
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 5px;
`;
const InputField = styled(BaseInput)`
  border-radius: 4px;
  border-width: 1px;
  border-style: solid;
  background-color: rgba(255, 255, 255, 1);
  padding: 0 15px;
  height: 45px;
  font-size: 14px;
  font-weight: 500;
  color: rgba(27, 27, 27, 1);
  border-color: rgba(227, 227, 227, 1);
`;

const SubmitButton = styled(BaseButton)`
  width: 100%;
  padding: 15px 25px;
  margin-bottom: 20px;
  border-color: rgba(177, 177, 177, 1);
  font-size: 18px;
  font-weight: 500;
  color: rgba(255, 255, 255, 1);
  background-color: var(--primary);
  outline: none;
  border: none;
`;
