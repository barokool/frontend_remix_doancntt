import styled from "@emotion/styled";
import React from "react";
import BaseButton from "~/components/Button";
import BaseInput from "~/components/Input";
import SocialButtons from "~/components/SocialButton";
import AuthLayout from "~/layouts/auth-layout";
import { Checkbox, CheckboxGroup } from "@chakra-ui/react";
import { useNavigate } from "@remix-run/react";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import type { ActionFunction } from "@remix-run/node";
import { redirect } from "@remix-run/node";
import { register } from "~/models/auth";

interface IFormValue {
  email: string;
  password: string;
  phoneNumber: string;
  displayName: string;
}

const schema = yup
  .object({
    email: yup
      .string()
      .required("This field is required")
      .email("Email is invalid"),
    password: yup
      .string()
      .required("This field is required")
      .min(6, "Please enter more than 6 characters"),
    phoneNumber: yup.string().required("This field is required"),
    displayName: yup.string().required("This field is required"),
  })
  .required();

export const action: ActionFunction = async ({ request }) => {
  const form = await request.formData();
  const email = form.get("email") as string;
  const password = form.get("password") as string;
  const phoneNumber = form.get("phoneNumber") as string;
  const displayName = form.get("displayName") as string;

  try {
    const data = await register({ email, password, displayName, phoneNumber });

    if (
      (data as any).msg === "User already existed" ||
      (data as any).message === "Internal server error"
    )
      return null;

    return redirect("/login");
  } catch (error) {
    console.log("error on action : ", error);
    return null;
  }
};

const Register = () => {
  const navigate = useNavigate();
  const formMethods = useForm<IFormValue>({
    defaultValues: {
      email: "",
      password: "",
      displayName: "",
      phoneNumber: "",
    },
    mode: "onBlur",
    resolver: yupResolver(schema),
  });

  const { register } = formMethods;

  return (
    <AuthLayout
      style={{ marginBottom: "50px" }}
      title="Sign up to BV Store"
      SocialButtons={
        <SocialButtons
          titleFacebook="Sign up with Facebook"
          titleGoogle="Sign up with Facebook"
        />
      }
      Form={
        <Form method="post">
          <Field>
            <Label htmlFor="fullname">Fullname</Label>
            <InputField
              type="text"
              placeholder="Enter your fullname"
              {...register("displayName")}
            />
          </Field>
          <Field>
            <Label htmlFor="email">Email address</Label>
            <InputField
              type="text"
              placeholder="Enter your Email Address"
              {...register("email")}
            />
          </Field>
          <Field>
            <Label htmlFor="password">Password</Label>
            <InputField
              type="text"
              placeholder="Enter your password"
              {...register("password")}
            />
          </Field>

          <Field>
            <Label htmlFor="phoneNumber">Phone number</Label>
            <InputField
              type="text"
              placeholder="Enter your phoneNumber"
              {...register("phoneNumber")}
            />
          </Field>

          <Field style={{ flexDirection: "row" }}>
            <Checkbox colorScheme={"blue"}>
              I agree to Kingify's Terms of Service and Privacy Policy
            </Checkbox>
          </Field>
          <Field style={{ flexDirection: "row" }}>
            <Checkbox colorScheme={"blue"} defaultChecked>
              Subscribe to receive news and updates from Kingify
            </Checkbox>
          </Field>
          <SubmitButton type="submit" title="Sign up" />
        </Form>
      }
      SubButton={
        <SubTitle>
          Already have an account?{" "}
          <SubSpan onClick={() => navigate("/login")}>Log in</SubSpan>
        </SubTitle>
      }
      description={
        <div style={{ textAlign: "center" }}>
          <Span>Or</Span>
        </div>
      }
    />
  );
};

export default Register;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  gap: 20px;
`;

const SubmitButton = styled(BaseButton)`
  width: 100%;
  padding: 15px 25px;
  margin-bottom: 20px;
  border-color: rgba(177, 177, 177, 1);
  font-size: 18px;
  font-weight: 500;
  color: rgba(255, 255, 255, 1);
  background-color: var(--primary);
  outline: none;
  border: none;
`;

const Label = styled.label`
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 5px;
`;

const Field = styled.div`
  display: flex;
  flex-direction: column;
`;

const InputField = styled(BaseInput)`
  border-radius: 4px;
  border-width: 1px;
  border-style: solid;
  background-color: rgba(255, 255, 255, 1);
  padding: 0 15px;
  height: 45px;
  font-size: 14px;
  font-weight: 500;
  color: rgba(27, 27, 27, 1);
  border-color: rgba(227, 227, 227, 1);
`;

const SubTitle = styled.h4`
  font-size: 16px;
  font-weight: 500;
`;

const SubSpan = styled.span`
  color: var(--primary);
  cursor: pointer;
`;

const Span = styled.span`
  font-size: 14px;
  font-weight: 500;
`;
