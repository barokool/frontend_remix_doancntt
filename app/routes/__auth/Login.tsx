import styled from "@emotion/styled";
import type { ActionFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { LoaderFunction, redirect } from "@remix-run/node";
import { useActionData, useNavigate } from "@remix-run/react";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import BaseButton from "~/components/Button";
import BaseInput from "~/components/Input";
import SocialButtons from "~/components/SocialButton";
import AuthLayout from "~/layouts/auth-layout";
import { login } from "~/models/auth";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { createUserSession } from "~/utils/auth";

interface IFormValue {
  email: string;
  password: string;
}

const schema = yup
  .object({
    email: yup
      .string()
      .required("This field is required")
      .email("Email is invalid"),
    password: yup
      .string()
      .required("This field is required")
      .min(6, "Please enter more than 6 characters"),
  })
  .required();

export const action: ActionFunction = async ({ request }) => {
  const form = await request.formData();
  const email = form.get("email") as string;
  const url = new URL(request.url);
  const redirectTo = url.searchParams.get("redirect");

  const password = form.get("password") as string;
  try {
    const data = await login({ email, password });

    console.log({ data });

    if (
      (data as any).msg ===
        "User || password wrong or this account does not exit" ||
      (data as any).message === "Internal server error"
    )
      return null;

    if (redirectTo)
      return createUserSession((data as any).accessToken, redirectTo);
    return createUserSession((data as any).accessToken, "/");
  } catch (error) {
    console.log("error on action : ", error);
    return null;
  }
};

const Login = () => {
  const navigate = useNavigate();
  const data = useActionData();

  const [error, setError] = useState(false);

  useEffect(() => {
    if (data === null) setError(true);
  }, [data]);

  const {
    register,
    handleSubmit,
    formState: { errors },
    trigger,
  } = useForm<IFormValue>({
    defaultValues: {
      email: "",
      password: "",
    },
    mode: "onBlur",
    resolver: yupResolver(schema),
  });

  const onClick = async (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    const wrongValidate = await trigger(["email", "password"]);
    if (!wrongValidate) e.preventDefault();
  };

  return (
    <AuthLayout
      title="Login to BV Store"
      SocialButtons={
        <SocialButtons
          titleFacebook="Login with Facebook"
          titleGoogle="Login with Google"
        />
      }
      // onSubmit={handleSubmit(onSubmit)}
      Form={
        <Form method="post">
          {error && (
            <Span style={{ textAlign: "left", color: "red" }}>
              Some thing is wrong, please try again !
            </Span>
          )}
          <Field>
            <Label htmlFor="email">Email address</Label>
            <InputField
              type="email"
              placeholder="Enter your Email Address"
              {...register("email")}
            />

            {errors.email && (
              <Span style={{ textAlign: "left", color: "red" }}>
                {errors.email.message}
              </Span>
            )}
          </Field>
          <Field>
            <Label htmlFor="password">Password</Label>
            <InputField
              type="password"
              placeholder="Enter your password"
              {...register("password")}
            />

            {errors.password && (
              <Span style={{ textAlign: "left", color: "red" }}>
                {errors.password.message}
              </Span>
            )}
          </Field>
          <SubmitButton
            onClick={(e) => onClick(e)}
            type="submit"
            title="Sign in"
          />
        </Form>
      }
      SubButton={
        <ButtonWrapper>
          <button onClick={() => navigate("/register")}>Sign up</button>
          <button onClick={() => navigate("/forgot-password")}>
            Forgor password?
          </button>
        </ButtonWrapper>
      }
      description={
        <div style={{ textAlign: "center" }}>
          <Span>Or</Span>
        </div>
      }
    />
  );
};
export default Login;

//Right Div

const Span = styled.span`
  font-size: 14px;
  font-weight: 500;
  text-align: center;
`;

const Label = styled.label`
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 5px;
`;

const Field = styled.div`
  display: flex;
  flex-direction: column;
`;

const InputField = styled(BaseInput)`
  border-radius: 4px;
  border-width: 1px;
  border-style: solid;
  background-color: rgba(255, 255, 255, 1);
  padding: 0 15px;
  height: 45px;
  font-size: 14px;
  font-weight: 500;
  color: rgba(27, 27, 27, 1);
  border-color: rgba(227, 227, 227, 1);
`;

const SubmitButton = styled(BaseButton)`
  width: 100%;
  padding: 15px 25px;
  margin-bottom: 20px;
  border-color: rgba(177, 177, 177, 1);
  font-size: 18px;
  font-weight: 500;
  color: rgba(255, 255, 255, 1);
  background-color: var(--primary);
  outline: none;
  border: none;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  gap: 20px;
`;

const ButtonWrapper = styled.div`
  display: flex;
  gap: 20px;
`;
