import {
  Center,
  Grid,
  GridItem,
  Heading,
  HStack,
  Input,
  InputGroup,
  InputRightAddon,
  Select,
  Table,
  TableContainer,
  Tag,
  TagCloseButton,
  TagLabel,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
  VStack,
} from "@chakra-ui/react";
import type { LoaderArgs } from "@remix-run/node";
import { redirect } from "@remix-run/node";
import { useFetcher, useLoaderData, useNavigate } from "@remix-run/react";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import BaseButton from "~/components/Button";
import { styled } from "~/design-system/stitches.config";
import NoData from "~/icons/Nodata";
import type { IProduct } from "~/models/products";
import { createTransaction } from "~/models/transaction";
import { getUserToken } from "~/utils/auth";
import { removeSpaceString } from "~/utils/functions/removeSpace";
import { getCartProducts } from "~/utils/localStorage";

export const loader = async ({ request }: LoaderArgs) => {
  const validToken = await getUserToken(request);

  if (!validToken) return redirect("/");
  return validToken;
};

const Cart = () => {
  const [product, setProduct] = useState<IProduct[]>([]);
  const [isPay, setIsPay] = useState(false);
  const navigate = useNavigate();
  const token = useLoaderData();

  useEffect(() => {
    const cart = getCartProducts();
    if (cart) setProduct(cart);
  }, []);

  let price = 0;
  product.forEach((item) => (price += Number(removeSpaceString(item.price))));

  const handleDeleteFromCart = (id: string) => {
    // delete product from local storage
    const cart = getCartProducts();
    const newCart = cart.filter((item) => item._id !== id);
    localStorage.setItem("cart", JSON.stringify(newCart));
    setProduct(newCart);
  };

  const handlePayment = async () => {
    const newsProducts = product.map((item) => item._id);
    console.log(newsProducts);

    const data = await createTransaction({ products: newsProducts }, token);

    if (data) {
      const clearProducts: IProduct[] = [];
      localStorage.setItem("cart", JSON.stringify(clearProducts));
      setProduct(clearProducts);
      setIsPay(true);
    }
  };

  return (
    <Wrapper>
      <Heading mt={12}>Payment easily and Fast</Heading>
      {!isPay ? (
        <Grid
          h={"100%"}
          templateRows="repeat(2, 1fr)"
          templateColumns="repeat(4, 1fr)"
          gap={4}
          mt={10}
        >
          <GridItem colSpan={3} rowSpan={2}>
            <TableContainer
              borderWidth={1}
              borderColor={"#E2E8F0"}
              borderRadius={4}
            >
              {/* {product.length > 0 ? ( */}
              <Table size={"lg"} variant="simple">
                <Thead>
                  <Tr>
                    <Th>Amount</Th>
                    <Th>Product</Th>
                    <Th isNumeric>Price</Th>
                    <Th>Action</Th>
                  </Tr>
                </Thead>
                <Tbody>
                  {product.length > 0 ? (
                    product.map((item) => (
                      <Tr key={item.slug}>
                        <Td>1</Td>
                        <Td>{item.name}</Td>
                        <Td isNumeric>{item.price}</Td>
                        <Td onClick={() => handleDeleteFromCart(item._id)}>
                          <Tag size="lg" colorScheme="red" borderRadius="full">
                            <TagLabel>Delete</TagLabel>
                            <TagCloseButton />
                          </Tag>
                        </Td>
                      </Tr>
                    ))
                  ) : (
                    <Center my={4} display={"flex"} gap={4}>
                      <NoData />
                      <p style={{ fontWeight: "600" }}>
                        There is no item in your cart right now!{" "}
                      </p>
                    </Center>
                  )}
                </Tbody>
              </Table>
            </TableContainer>
          </GridItem>

          <GridItem rowSpan={2} colSpan={1}>
            <VStack>
              <BoxFrame>
                <VStack alignItems={"normal"} padding={4}>
                  <h3>Select your district</h3>
                  <InputGroup>
                    <Input placeholder="Your coupon" focusBorderColor="none" />
                    <InputRightAddon
                      children="Apply"
                      bgColor={"rgba(51, 204, 204, 1)"}
                      color={"white"}
                      fontWeight={700}
                      _hover={{ cursor: "pointer" }}
                    />
                  </InputGroup>
                  {/* <Select placeholder="Select option">
                    <option value="option1">Option 1</option>
                    <option value="option2">Option 2</option>
                    <option value="option3">Option 3</option>
                  </Select> */}
                </VStack>
              </BoxFrame>
              <BoxFrame>
                <VStack alignItems={"normal"} padding={4}>
                  <HStack justifyContent={"space-between"}>
                    <h3>Total Price</h3>
                    <span>
                      USD : <span>{price}</span>
                    </span>
                  </HStack>
                  <HStack justifyContent={"space-between"}>
                    <h3>Discount</h3>
                    <span>USD : 0</span>
                  </HStack>
                  <HStack justifyContent={"space-between"}>
                    <h3>Total </h3>
                    <strong>{price}</strong>
                  </HStack>
                </VStack>
              </BoxFrame>
              <PaidButton
                onClick={() => handlePayment()}
                disabled={!(product.length > 0)}
                isDisabled={!(product.length > 0)}
                title="Pay now"
              />
            </VStack>
          </GridItem>
        </Grid>
      ) : (
        <Center
          justifyContent={"center"}
          alignItems={"center"}
          display={"flex"}
          flexDirection={"column"}
          height={"100%"}
          gap={4}
        >
          <NoData />
          <p style={{ fontWeight: "600" }}>Payment successfully!</p>
          <PaidButton
            css={{ width: "max-content" }}
            onClick={() => navigate("/history-buy")}
            title="View your history purchase"
          />
        </Center>
      )}
    </Wrapper>
  );
};

export default Cart;

const PaidButton = styled(BaseButton, {
  backgroundColor: "var(--primary)",
  width: "100%",
  color: "white",
  fontWeight: "700",
  fontSize: "24px",
  border: "none !important",
  "&:hover": {
    opacity: "0.7",
  },

  variants: {
    isDisabled: {
      true: {
        opacity: "0.7",
      },
      false: {
        opacity: "1",
      },
    },
  },
});

const Wrapper = styled("main", {
  maxWidth: "1280px",
  width: "100vw",
  margin: "0 auto",
  height: "100vh",
});

const BoxFrame = styled("div", {
  borderWidth: "1px",
  borderColor: "#e2e8f0",
  borderRadius: "4px",
  width: "100%",
});
