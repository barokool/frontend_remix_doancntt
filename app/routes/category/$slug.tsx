import styled from "@emotion/styled";
import type { LoaderFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import {
  NavLink,
  useLoaderData,
  useLocation,
  useMatches,
} from "@remix-run/react";
import React, { CSSProperties } from "react";
import Banner from "~/components/Banner";
import BreadCrumb from "~/components/BreadCrumb";
import ProductCard from "~/components/ProductCard";
import ArrowDown from "~/icons/ArrowDown";
import MiddleLayout from "~/layouts/general-layout/MiddleLayout";
import type { ICategory } from "~/models/category";
import { getAllCategories } from "~/models/category";
import type { IProduct } from "~/models/products";
import { getAllProductByFilter } from "~/models/products";
import { upperFirstLetter } from "~/utils/functions/upperFirstLetter";

export const loader: LoaderFunction = async ({ request, params }) => {
  console.log("params : ", params.slug);
  const categoriesData = await getAllCategories();

  const categories: ICategory[] = [];
  categoriesData.map((category) => {
    if (!category.parent) categories.push(category);
  });

  const categoryOnParam = categories.find(
    (category) => category.name === `${params.slug}`
  );

  const products = await getAllProductByFilter(categoryOnParam?._id || "");

  return json({ categories, products });
};

const CategoryDetail = () => {
  const { categories, products } = useLoaderData<{
    categories: ICategory[];
    products: IProduct[];
  }>();

  const { pathname } = useLocation();

  console.log(pathname);

  const path = pathname.split("/");

  const productData: IProduct[] = (products as any)?.data;

  return (
    <MiddleLayout>
      <Banner />
      <GridLayout>
        <div>
          {categories.map((item, index) => {
            return (
              <CategoryItem
                key={index}
                style={
                  item.slug === path[2]
                    ? {
                        backgroundColor: "rgba(239, 250, 250, 1)",
                      }
                    : { backgroundColor: "white" }
                }
              >
                <NavLink to={`/category/${item.slug}`}>
                  <p>{upperFirstLetter(item.name || "")}</p>
                </NavLink>
                <ArrowDown />
              </CategoryItem>
            );
          })}
        </div>
        <div>
          <BreadCrumb
            path={path}
            css={{ marginBottom: 20 }}
            label={"Category"}
          />
          {productData.length > 0 ? (
            <ProductWrapper>
              {productData &&
                productData.map((item) => {
                  return <ProductCard product={item} />;
                })}
            </ProductWrapper>
          ) : (
            <div>
              <p style={{ fontWeight: "600", fontSize: "24px" }}>
                This item currently is not available ! Sorry for this{" "}
              </p>
            </div>
          )}
        </div>
      </GridLayout>
    </MiddleLayout>
  );
};

export default CategoryDetail;

const GridLayout = styled.div`
  display: grid;
  grid-template-columns: 250px 1fr;
  margin-top: 40px;
  grid-column-gap: 20px;
`;

const CategoryItem = styled.div`
  padding: 10px;
  font-weight: 700;
  font-size: 16px;
  user-select: none;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-radius: 4px;
`;

const ProductWrapper = styled.div`
  display: flex;
  gap: 20px;
  flex-wrap: wrap;
  /* width: calc((100% - 60px) / 4); */
`;
