import styled from "@emotion/styled";
import { Outlet } from "@remix-run/react";
import BaseButton from "~/components/Button";
import { Tabs, TabList, TabPanels, Tab, TabPanel } from "@chakra-ui/react";
import HomeLayout from "~/layouts/home-layout/HomeLayout";
import type { LoaderFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import type { ICategory } from "~/models/category";
import { getAllCategories } from "~/models/category";
import HomeIcon from "~/icons/BannerHome";

export const loader: LoaderFunction = async ({ request }) => {
  const categoriesData = await getAllCategories();

  const categories: ICategory[] = [];
  categoriesData.map((category) => {
    if (!category.parent) categories.push(category);
  });

  return json({ categories });
};

export default function Index() {
  return (
    <HomeLayout>
      <Wrapper>
        <LeftDiv>
          <Title>Buy and sell products with an easily way</Title>
          <SecondTitle>
            Your design ideas and our platform are a match made in heaven
          </SecondTitle>
          <ButtonWrapper>
            <Button title="Start selling" />
            <Button
              style={{
                backgroundColor: "rgba(238,238,238,1)",
                color: "rgba(34,34,34,1)",
              }}
              title="Buy now"
            />
          </ButtonWrapper>
          <Para>Hassle free - Quality print products - No minimum order</Para>
          <div>
            <h3
              style={{
                fontSize: "20px",
                fontWeight: 700,
                color: "rgba(34,34,34,1)",
              }}
            >
              Over 1 million items shipped every month
            </h3>
          </div>
        </LeftDiv>
        <RightDiv>
          <div>
            <HomeIcon />
          </div>
        </RightDiv>
      </Wrapper>
      <Tabs isLazy mt={5}>
        <TabList>
          <Tab>Today</Tab>
          <Tab>Hot Product</Tab>
        </TabList>

        <TabPanels>
          <TabPanel>
            <Outlet />
          </TabPanel>
          <TabPanel>
            <p>two!</p>
          </TabPanel>
          <TabPanel>
            <p>three!</p>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </HomeLayout>
  );
}

const Wrapper = styled.div`
  grid-template-columns: 1fr 1fr;
  display: grid;
  margin-top: 25px;
`;
// left div
const LeftDiv = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1 1 0;
  gap: 20px;
`;

const Title = styled.h1`
  font-size: 48px;
  font-weight: 700;
  color: rgba(34, 34, 34, 1);
  line-height: 1.2;
`;

const SecondTitle = styled.h2`
  font-size: 24px;
  font-weight: 300;
  color: rgba(85, 85, 85, 1);
`;

const ButtonWrapper = styled.div`
  display: flex;
  gap: 20px;
  justify-content: flex-start;
`;

const Button = styled(BaseButton)`
  width: max-content;
  padding: 15px 25px;
  border-color: rgba(177, 177, 177, 1);
  font-size: 20px;
  font-weight: 500;
  color: rgba(255, 255, 255, 1);
  background-color: var(--primary);
  outline: none;
  border: none;
  border-radius: 5px;
`;

const Para = styled.p`
  font-weight: 14px;
  font-weight: 500;
  color: rgba(85, 85, 85, 1);
`;
// end left div
const RightDiv = styled.div``;
