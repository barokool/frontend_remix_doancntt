import { Heading } from "@chakra-ui/react";
import type { LoaderArgs } from "@remix-run/node";
import { json, redirect } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import React from "react";
import { styled } from "~/design-system/stitches.config";
import HistoryCart from "~/layouts/history-purchase/HistoryCard";
import type { ITransaction } from "~/models/transaction";
import { getAllTransaction } from "~/models/transaction";
import { getUserToken } from "~/utils/auth";

export const loader = async ({ request }: LoaderArgs) => {
  const validToken = await getUserToken(request);

  if (!validToken) return redirect("/");

  const transactions = await getAllTransaction(validToken);

  if (transactions) return json({ transactions });
  return json({ transactions: {} });
};

const HistoryBuy = () => {
  const loaderData = useLoaderData<{
    transactions: { result: ITransaction[]; count: number };
  }>();

  return (
    <Wrapper>
      <Heading textAlign={"center"} mt={12}>
        Your History Purchase
      </Heading>
      <section>
        {loaderData
          ? loaderData?.transactions?.result?.map((transaction, idx) => {
              return (
                <HistoryCart
                  transaction={transaction}
                  key={`${Math.random()}`}
                />
              );
            })
          : null}
      </section>
    </Wrapper>
  );
};

export default HistoryBuy;

const Wrapper = styled("main", {
  display: "grid",
  minHeight: "50vh",
  gap: "$x6",

  "& > section": {
    maxWidth: "1440px",
    width: "1280px",
    margin: "0 auto",
    display: "grid",
    gap: "$x4",
  },
});

const Title = styled("h1", {
  mediumBody: "$20px",
  textAlign: "center",
  marginTop: "20px",
});
