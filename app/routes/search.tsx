import type { LoaderArgs } from "@remix-run/node";
import { json } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import React from "react";
import Pagination from "~/components/Pagination";
import ProductCard from "~/components/ProductCard";
import { styled } from "~/design-system/stitches.config";
import type { IProduct } from "~/models/products";
import { getAllProduct } from "~/models/products";

export const loader = async ({ request }: LoaderArgs) => {
  const url = new URL(request.url);
  const searchParam = new URLSearchParams(url.search);
  const keyword = searchParam.get("query")?.split(" ").join("-");
  const page = +(searchParam.get("page") || 1);

  if (searchParam.get("query")?.length || 0 > 0) {
    try {
      const products = await getAllProduct(page, 10, keyword);

      return json({ products, status: true });
    } catch (error) {
      console.log("errrror : ", error);
      return json({ products: [], status: false });
    }
  }
  return json({ products: [] });
};

const Search = () => {
  const { products } = useLoaderData<{
    products: { result: IProduct[]; count: number };
    status: boolean;
  }>();

  return (
    <Wrapper>
      {products && products?.result?.length > 0 ? (
        <>
          <Heading>{products?.result?.length.toString() + " Found"}</Heading>
          <Container>
            {products?.result?.map((product, idx) => {
              return (
                <ProductCard
                  wrapperStyled={{
                    css: {
                      width: "calc((100% - 400px) / 1)",
                      margin: "0 auto",
                    },
                  }}
                  product={product}
                  key={idx}
                />
              );
            })}
          </Container>
          <Pagination itemPerPage={12} totalItems={products?.result?.length} />
        </>
      ) : (
        <Container>
          <p>Type anything to search a product</p>
        </Container>
      )}
    </Wrapper>
  );
};

export default Search;

const Wrapper = styled("section", {
  maxWidth: "1440px",
  margin: "0 auto",
  minHeight: "100vh",
});

const Container = styled("div", {
  width: "720px",
  marginInline: "auto",
  marginTop: "40px",

  display: "grid",
  gap: "$x6",
});

const Heading = styled("h1", {
  boldBody: "$16px",
  textAlign: "center",
});
