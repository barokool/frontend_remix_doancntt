import { Grid, GridItem } from "@chakra-ui/react";
import type { LoaderFunction } from "@remix-run/node";
import { Link, Outlet, useLoaderData, useLocation } from "@remix-run/react";
import React, { useState } from "react";
import BreadCrumb from "~/components/BreadCrumb";
import { styled } from "~/design-system/stitches.config";
import ProtectedProfile from "~/icons/Protected.profile";
import UserProfile from "~/icons/User.profile";
import { requireUserValidToken } from "~/utils/auth";

export const loader: LoaderFunction = async ({ request }) => {
  return requireUserValidToken(request);
};

const Profile = () => {
  const { pathname } = useLocation();
  console.log(pathname);
  const data = useLoaderData();
  console.log(data);

  return (
    <Wrapper>
      <BreadCrumb
        css={{ marginTop: 20 }}
        path={pathname.split("/")}
        label={"profile"}
      />
      <Grid mt={8} templateColumns="repeat(4, 1fr)" gap={8}>
        <GridItem rowSpan={2} colSpan={1}>
          <LeftWrap>
            {settings.map((item, index) => {
              return (
                <ContentWrap
                  to={item.to}
                  active={pathname === item.to}
                  key={index}
                >
                  {item.icon}
                  <p>{item.content}</p>
                </ContentWrap>
              );
            })}
          </LeftWrap>
        </GridItem>
        <GridItem rowSpan={2} colSpan={3}>
          <Outlet />
        </GridItem>
      </Grid>
    </Wrapper>
  );
};

export default Profile;

const Wrapper = styled("main", {
  maxWidth: "1280px",
  width: "100vw",
  margin: "0 auto",
});

const LeftWrap = styled("div", {
  border: "1px solid $neutral500",
  borderRadius: "$space$x1",
  paddingBlock: "$x6",
  display: "grid",
  gap: "$x4",
});

const ContentWrap = styled(Link, {
  display: "flex",
  gap: "$x2",
  marginInline: "$x4",
  paddingBlock: "$x3",
  paddingInline: "$x4",
  p: {
    mediumBody: "$16px",
  },
  "&:hover": {
    p: {
      color: "$primaryGreen500",
    },
    path: {
      fill: "$primaryGreen500",
    },
  },
  variants: {
    active: {
      true: {
        borderLeft: "5px solid $primaryGreen600",
        p: {
          color: "$primaryGreen600",
        },
        path: {
          fill: "$primaryGreen600",
        },
      },
    },
  },
});

const settings = [
  {
    icon: <UserProfile />,
    content: "General ",
    to: "/profile",
  },
  {
    icon: <ProtectedProfile />,
    content: "Account setting",
    to: "/profile/account-setting",
  },
];
