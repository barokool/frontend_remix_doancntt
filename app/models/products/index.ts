import { API_URL } from "~/constants/baseAPI";

export interface IProduct {
  _id: string;
  category: string;
  createdAt: Date;
  createdBy: string;
  images: string[];
  name: string;
  price: string;
  status: string;
  updatedAt: Date;
  slug: string;
}

export const getAllProduct = async (
  page: number,
  limit: number,
  keyword?: string
): Promise<{ result: IProduct[]; count: number }> => {
  const data = await fetch(
    `${API_URL}/products?page=${page}&limit=${limit}&keyword=${keyword}`
  );

  return await data.json();
};

export const getProductById = async (id: string) => {
  const data = await fetch(`${API_URL}/${id}`);
  return await data.json();
};

export const getProductBySlug = async (slug: string) => {
  const data = await fetch(`${API_URL}/products/${slug}`);
  return await data.json();
};

export const getAllProductByFilter = async (categoryId: string) => {
  if (!categoryId) return null;
  const data = await fetch(`${API_URL}/products/filter/${categoryId}`);

  return await data.json();
};
