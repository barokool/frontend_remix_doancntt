import { API_URL } from "~/constants/baseAPI";
import type {
  AdminPermissionEnum,
  PermissionEnum,
  RoleEnum,
} from "~/constants/enum";

export interface IUser {
  _id: string;
  displayName: string;
  email: string;
  password: string;
  phoneNumber: string;
  address: string;
  role: RoleEnum;
  permissions: PermissionEnum;
  createdAt: Date;
  updatedAt: Date;
}

export interface IAdmin {
  _id: string;
  displayName: string;
  email: string;
  phoneNumber: string;
  password: string;
  role: RoleEnum;
  permissions: AdminPermissionEnum;
  createdAt: Date;
  updatedAt: Date;
}

export interface ILoginInput {
  email: string;
  password: string;
}

export interface IRegisterInput {
  email: string;
  phoneNumber: string;
  password: string;
  displayName: string;
}

export interface IPayload {
  id: string;
  accessToken: string;
}

export interface IUpdateUser {
  displayName?: string;
  email?: string;
  password?: string;
  phoneNumber?: string;
  address?: string;
}

export interface IUpdateUserPass {
  password: string;
  newPassword: string;
}

export const login = async (
  input: ILoginInput
): Promise<IPayload | { statusCode: number; message: string }> => {
  try {
    const data = await fetch(`${API_URL}/auth/signIn`, {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(input),
    });
    return await data.json();
  } catch (error) {
    console.log(error);
    return { statusCode: 500, message: `${error}` };
  }
};

export const register = async (
  input: IRegisterInput
): Promise<IUser | { statusCode: number; message: string }> => {
  try {
    const data = await fetch(`${API_URL}/auth/signUp`, {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(input),
    });
    return await data.json();
  } catch (error) {
    console.log(error);
    return { statusCode: 500, message: `${error}` };
  }
};

export const updateAccount = async (input: IUpdateUser, token: string) => {
  const bearer = "Bearer " + token;
  try {
    const data = await fetch(`${API_URL}/auth/update-user`, {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: bearer,
      },
      body: JSON.stringify(input),
    });
    return await data.json();
  } catch (error) {
    console.log("errorrrr : ", error);
    return error;
  }
};

export const updatePassword = async (input: IUpdateUserPass, token: string) => {
  const bearer = "Bearer " + token;
  try {
    const data = await fetch(`${API_URL}/auth/update-password`, {
      method: "PUT",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: bearer,
      },
      body: JSON.stringify(input),
    });

    return await data.json();
  } catch (error) {}
};
