import { API_URL } from "~/constants/baseAPI";

export const getUserByEmail = async (email: string | null) => {
  if (!email) return null;
  const data = await fetch(`${API_URL}/auth/email/${email}`);

  return await data.json();
};
