import { API_URL } from "~/constants/baseAPI";
import type { TransactionStatusEnum } from "~/constants/enum";
import type { IUser } from "../auth";
import type { IProduct } from "../products";

export interface ITransaction {
  products?: IProduct[];
  buyer?: IUser;
  status?: TransactionStatusEnum;
  totalPrice?: number;
  createAt: Date;
}

export interface CreateTransactionInput {
  products: string[];
}

export const getAllTransaction = async (token: string) => {
  const bearer = "Bearer " + token;
  try {
    const data = await fetch(`${API_URL}/transaction`, {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: bearer,
      },
    });

    return await data.json();
  } catch (error) {
    console.log("errorr : ", error);
    return { result: [], count: 0 };
  }
};

export const createTransaction = async (
  input: CreateTransactionInput,
  validToken: string
) => {
  console.log("bere");

  const bearer = "Bearer " + validToken;
  try {
    const data = await fetch(`${API_URL}/transaction/create`, {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: bearer,
      },
      body: JSON.stringify(input),
    });
    console.log("success");

    return await data.json();
  } catch (error) {
    console.log("log");

    console.log(error);
    return { statusCode: 500, message: `${error}` };
  }
};
