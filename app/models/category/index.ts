import { API_URL } from "~/constants/baseAPI";

export interface ICategory {
  _id: string;
  name?: string;
  hasChild?: boolean;
  parent?: string;
  slug?: string;
  createdAt: string;
  updatedAt: string;
}

export const getAllCategories = async (): Promise<ICategory[]> => {
  const data = await fetch(`${API_URL}/categories`);

  return await data.json();
};
