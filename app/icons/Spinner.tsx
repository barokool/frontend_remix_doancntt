import React from "react";
import { keyframes, styled } from "~/design-system/stitches.config";

function Spinner() {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      className="spinner"
      viewBox="0 0 66 66"
    >
      <Circle
        cx="33"
        cy="33"
        r="30"
        fill="none"
        strokeLinecap="round"
        strokeWidth="6"
        className="path"
      ></Circle>
    </SVG>
  );
}

export default Spinner;

const rotator = keyframes({
  "0%": {
    transform: "rotate(0deg)",
  },
  "100%": {
    transform: "rotate(270deg)",
  },
});

const colors = keyframes({
  "0%": {
    stroke: "$primaryGreen500",
  },
  "25%": {
    stroke: "$primaryPurple400",
  },
  "50%": {
    stroke: "$blue500",
  },
  "75%": {
    stroke: "$primaryPurple400",
  },
  "100%": {
    stroke: "$primaryGreen500",
  },
});

const dash = keyframes({
  "0%": {
    "stroke-dashoffset": 187,
  },
  "50%": {
    "stroke-dashoffset": 46.75,
    transform: "rotate(135deg)",
  },
  "100%": {
    "stroke-dashoffset": 187,
    transform: "rotate(450deg)",
  },
});

const SVG = styled("svg", {
  animation: `${rotator} 1.4s linear infinite`,
});

const Circle = styled("circle", {
  "stroke-dasharray": 187,
  "stroke-dashoffset": 0,
  "-webkit-transform-origin": "center",
  "-ms-transform-origin": "center",
  "transform-origin": "center",
  animation: `${dash} 1.4s ease-in-out infinite, ${colors} 2s ease-in-out infinite`,
});
