import React from "react";

function ArrowDown() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        fill="#000"
        d="M17 9.17a1 1 0 00-1.41 0L12 12.71 8.46 9.17a1 1 0 10-1.41 1.42l4.24 4.24a1.001 1.001 0 001.42 0L17 10.59a.999.999 0 000-1.42z"
      ></path>
    </svg>
  );
}

export default ArrowDown;
