import React from "react";

function Cart() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        stroke="#14181F"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M8 8a4 4 0 118 0M4.721 9.671A2 2 0 016.694 8h10.612a2 2 0 011.973 1.671l1.333 8A2 2 0 0118.639 20H5.361a2 2 0 01-1.973-2.329l1.333-8z"
      ></path>
    </svg>
  );
}

export default Cart;
