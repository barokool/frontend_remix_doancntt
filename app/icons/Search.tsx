import React from "react";

function Search() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        stroke="#14181F"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M21 21l-4.343-4.343m0 0A8 8 0 105.343 5.343a8 8 0 0011.314 11.314z"
      ></path>
    </svg>
  );
}

export default Search;
