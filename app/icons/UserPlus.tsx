import React from "react";

function UserPlus() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        stroke="#14181F"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M3 21c0-2.21 3.134-4 7-4s7 1.79 7 4M19 17v-6M16 14h6M10 13a5 5 0 100-10 5 5 0 000 10z"
      ></path>
    </svg>
  );
}

export default UserPlus;
