import React from "react";

function RightArrow() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        fill="#000"
        d="M14.83 11.29l-4.24-4.24a.999.999 0 00-1.71.705 1 1 0 00.29.705L12.71 12l-3.54 3.54a1 1 0 000 1.41 1 1 0 00.71.29 1 1 0 00.71-.29l4.24-4.24a.999.999 0 000-1.42z"
      ></path>
    </svg>
  );
}

export default RightArrow;
