import { Grid, Progress } from "@chakra-ui/react";
import type { SerializeFrom } from "@remix-run/node";
import React from "react";
import { styled } from "~/design-system/stitches.config";
import type { ITransaction } from "~/models/transaction";

type Props = {
  transaction: SerializeFrom<ITransaction>;
};

const HistoryCart = ({ transaction }: Props) => {
  const buyedDate = new Date(`${transaction.createAt}`);
  return (
    <Wrapper>
      <Flex css={{ justifyContent: "space-between" }}>
        <Span>{transaction.buyer?.displayName}</Span>
        <Span>Amount : {transaction.totalPrice} $</Span>
      </Flex>
      <DateExpired>
        <p>Buyed at : {buyedDate.toDateString()}</p>
      </DateExpired>
      <Grid>
        {/* <Title>{transaction}</Title> */}
        <div>
          <Span>{transaction.status?.toString() || "Pending"}</Span>
          <Progress value={80} isIndeterminate borderRadius={"12px"} />
        </div>
        <Grid gap={2}>
          {transaction &&
            transaction?.products?.map((product, idx) => {
              return (
                <div key={idx}>
                  <Title>{product.name}</Title>
                  <span>{product.price}</span>
                </div>
              );
            })}
        </Grid>
      </Grid>
    </Wrapper>
  );
};

export default HistoryCart;

const Wrapper = styled("div", {
  width: "400px",
  margin: "0 auto",
  boxShadow: "1px 4px 14px -3px rgba(0,0,0,0.79)",
  height: "200px",
  borderRadius: "4px",
  paddingInline: "20px",
  paddingBottom: "10px",
  display: "grid",
  gap: "$x1",
  cursor: "pointer",
  "&:hover": {
    boxShadow: "-1px 6px 32px -9px rgba(0,0,0,0.63)",
  },
});

const Flex = styled("div", {
  display: "flex",
  alignItems: "center",
});

const Span = styled("span", {
  mediumBody: "$12px",
});

const DateExpired = styled("span", {
  borderRadius: "12px",
  border: "1px solid $neutral500",
  padding: "0 10px",
  mediumBody: "$12px",
  display: "flex",
  alignItems: "center",
});

const Title = styled("h2", {
  mediumBody: "$14px",
  lineLimit: 1,
});
