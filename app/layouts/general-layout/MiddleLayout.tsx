import styled from "@emotion/styled";
import React, { ReactNode } from "react";

interface IMiddleLayoutProps {
  children: ReactNode;
}
const MiddleLayout: React.FC<IMiddleLayoutProps> = ({ children }) => {
  return <Wrapper>{children}</Wrapper>;
};

export default MiddleLayout;

const Wrapper = styled.main`
  max-width: 1280px;
  width: 100vw;
  margin: 0 auto;
`;
