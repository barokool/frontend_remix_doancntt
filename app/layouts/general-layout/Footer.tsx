import { Input, InputGroup } from "@chakra-ui/react";
import styled from "@emotion/styled";
import React from "react";
import MasterCard from "~/icons/MasterCard";
import Paypal from "~/icons/Paypal";
import Visa from "~/icons/Visa";

const Footer = () => {
  return (
    <Wrapper>
      <Container>
        <FormContainer>
          <InputGroup>
            <Input
              size={"md"}
              backgroundColor={"white"}
              placeholder="Subscribe your email to our new letters!"
            />
          </InputGroup>
        </FormContainer>
        <div>
          <p>Payment methods</p>
          <PaymentUL>
            {paymentMethods.map((payment) => (
              <li key={Math.random()}>{payment.icon}</li>
            ))}
          </PaymentUL>
        </div>
        <div>
          <p>Copyright © 2022 BV Store by Barogia.,. All rights reserved.</p>
        </div>
      </Container>
    </Wrapper>
  );
};

export default Footer;

const paymentMethods = [
  {
    icon: <Paypal />,
  },
  {
    icon: <Visa />,
  },
  {
    icon: <MasterCard />,
  },
];

const Wrapper = styled.footer`
  background-color: rgba(248, 248, 248, 1);
  padding: 20px 0;
`;
const Container = styled.div`
  width: 100vw;
  max-width: 1280px;
  margin: 0 auto;
  display: flex;
  gap: 20px;
  flex-direction: column;
`;
const FormContainer = styled.form`
  max-width: 600px;
`;

const PaymentUL = styled.ul`
  display: flex;
  flex-direction: row;
  gap: 20px;
  list-style: none;
  margin-top: 10px;
  li > svg {
    width: 50px;
    height: 50px;
  }
`;
