import {
  Avatar,
  Button,
  HStack,
  Input,
  InputGroup,
  InputLeftElement,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Text,
} from "@chakra-ui/react";
import type { LoaderFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import {
  Link,
  useLoaderData,
  useNavigate,
  useSearchParams,
} from "@remix-run/react";
import React from "react";
import { styled } from "~/design-system/stitches.config";
import Cart from "~/icons/Cart";
import RightArrow from "~/icons/RightArrow";
import Search from "~/icons/Search";
import User from "~/icons/User";
import UserPlus from "~/icons/UserPlus";

const Header = () => {
  const navigate = useNavigate();
  const [params] = useSearchParams();
  const { user } = useLoaderData();

  const onSubmit = () => {
    navigate("/search");
  };

  const onLogout = () => {
    const cart = JSON.parse(localStorage.getItem("cart")!);
    if (cart) {
      localStorage.removeItem("cart");
    }
  };

  return (
    <Container>
      <Link to={"/"}>
        {/* <Logo /> */}
        <ImageDiv
          src="https://cdn.shopify.com/s/files/1/0496/4892/6873/files/logo_bv_150x@2x.png?v=1613781885"
          alt="logo-bv"
        />
      </Link>
      <SearchContainer>
        <SearchBox onSubmit={onSubmit}>
          <InputGroup _focus={{ border: "none" }}>
            <InputLeftElement pointerEvents="none" children={<Search />} />
            <Input
              focusBorderColor="none"
              placeholder="Search BV Store"
              name="query"
              defaultValue={params.get("query") || ""}
            />
          </InputGroup>
        </SearchBox>
        {/* If you add the size prop to `InputGroup`, it'll pass it to all its children. */}
      </SearchContainer>
      <ButtonContainer>
        {!user ? (
          <>
            <Link to={"/login"}>
              <Menu>
                <MenuButton as={Button}>
                  <HStack>
                    <User />
                    <Text>Sign in</Text>
                  </HStack>
                </MenuButton>
              </Menu>
            </Link>
            <Link to={"/register"}>
              <Menu>
                <MenuButton as={Button}>
                  <HStack>
                    <UserPlus />
                    <Text>Sign up</Text>
                  </HStack>
                </MenuButton>
              </Menu>
            </Link>
          </>
        ) : (
          <Menu>
            <MenuButton as={Button} rightIcon={<RightArrow />}>
              <HStack>
                <Avatar
                  src="https://bit.ly/sage-adebayo"
                  size="xs"
                  name={user.user.displayName}
                  ml={-1}
                  mr={2}
                />
                <Text>{user.user.displayName}</Text>
              </HStack>
            </MenuButton>
            <MenuList>
              <Link to="/profile">
                <MenuItem minH="48px">
                  <span>Profile</span>
                </MenuItem>
              </Link>
              <Link to="/history-buy">
                <MenuItem minH="48px">
                  <span>History purchase</span>
                </MenuItem>
              </Link>
              <MenuItem onClick={() => onLogout()} minH="40px">
                <form action="/logout" method="post">
                  <button type="submit">Log out</button>
                </form>
              </MenuItem>
            </MenuList>
          </Menu>
        )}
        <Link to={"/cart"}>
          <Menu>
            <MenuButton as={Button}>
              <HStack>
                <Cart />
                <Text>Cart </Text>
              </HStack>
            </MenuButton>
          </Menu>
        </Link>
      </ButtonContainer>
    </Container>
  );
};

export default Header;

const Container = styled("header", {
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  width: "100vw",
  maxWidth: "1280px",
  margin: "0 auto",
  padding: "20px 0",
  "@tablet": {
    maxWidth: "700px",
    margin: "0 auto",
  },
});
const SearchContainer = styled("div", {
  maxWidth: "700px",
  width: "100%",
  "@tablet": {
    maxWidth: "700px",
    minWidth: "250px",
  },
  "@mobile": {
    display: "none",
  },
});

const ImageDiv = styled("img", {
  minWidth: "100px",
  width: "100%",
  maxWidth: "133px",
});
const SearchBox = styled("form", {
  width: "100%",
  height: "100%",
  display: "flex",
});

const ButtonContainer = styled("div", {
  display: "flex",
  gap: "20px",
  alignItems: "center",
});

export let loader: LoaderFunction = ({ request }) => {
  const url = new URL(request.url);
  const search = new URLSearchParams(url.search);

  return json(search.get("query")); // Filters tweets based on the query
};
