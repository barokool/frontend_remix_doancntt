import { useLocation } from "@remix-run/react";
import type { ReactNode } from "react";
import React from "react";
import Footer from "./general-layout/Footer";
import Header from "./general-layout/Header";

const Layout: React.FC<{ children: ReactNode }> = ({ children }) => {
  const { pathname } = useLocation();

  return (
    <>
      {pathname === "/register" ||
      pathname === "/login" ||
      pathname === "/forgot-password" ? (
        <div>{children}</div>
      ) : (
        <>
          <Header />
          <div style={{ marginBottom: "50px", height: "100%" }}>{children}</div>

          <Footer />
        </>
      )}
    </>
  );
};

export default Layout;
