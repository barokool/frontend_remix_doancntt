import styled from "@emotion/styled";
import React, { ReactElement } from "react";
interface IAuthLayoutProps extends React.HTMLAttributes<HTMLDivElement> {
  title?: string;
  SocialButtons?: ReactElement;
  description?: ReactElement;
  Form?: ReactElement;
  SubButton?: ReactElement;
}

const AuthLayout: React.FC<IAuthLayoutProps> = (props) => {
  const { Form, SocialButtons, SubButton, description, title, ...prop } = props;
  return (
    <FormContainer {...prop}>
      <FormWrapper>
        <Title>{title}</Title>
        <div>{SocialButtons}</div>
        {description}
        {Form}
        {SubButton}
      </FormWrapper>
    </FormContainer>
  );
};

export default AuthLayout;

const FormContainer = styled.div`
  max-width: 550px;
  height: 100%;
  grid-area: "form";
  margin: 0 auto;
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

const FormWrapper = styled.div`
  max-width: 450px;
  width: 100%;
  margin: 0 auto;
`;

const Title = styled.h1`
  font-size: 36px;
  font-weight: 700;
  margin-bottom: 20px;
`;
