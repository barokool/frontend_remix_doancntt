import styled from "@emotion/styled";
import { Link, useLoaderData } from "@remix-run/react";
import type { ReactNode } from "react";
import React from "react";
import type { ICategory } from "~/models/category";
import { upperFirstLetter } from "~/utils/functions/upperFirstLetter";

interface IHomeLayoutProps {
  children: ReactNode;
}

const HomeLayout: React.FC<IHomeLayoutProps> = ({ children }) => {
  const { categories } = useLoaderData<{ categories: ICategory[] }>();

  return (
    <Wrapper>
      <ProductContainer>
        {categories &&
          categories.map((item, index) => {
            return (
              <Link key={index} to={`/category/${item.name}`}>
                <ProductCategory>
                  {upperFirstLetter(item.name || "")}
                </ProductCategory>
              </Link>
            );
          })}
      </ProductContainer>
      {children}
    </Wrapper>
  );
};

export default HomeLayout;

const Wrapper = styled.main`
  max-width: 1280px;
  width: 100vw;
  margin: 0 auto;
`;

const ProductContainer = styled.div`
  display: flex;
  gap: 20px;
  padding: 10px 0;
  border-top: 1px solid;
  border-color: rgba(227, 227, 227, 1);
`;

const ProductCategory = styled.p`
  font-size: 16px;
  &:hover {
    cursor: pointer;
  }
`;
