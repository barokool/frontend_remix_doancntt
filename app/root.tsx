import type {
  LinksFunction,
  LoaderFunction,
  MetaFunction,
} from "@remix-run/node";
import { json } from "@remix-run/node";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useLoaderData,
} from "@remix-run/react";
import Layout from "./layouts/Layout";
import styles from "~/styles/global.css";
import { ServerStyleContext, ClientStyleContext } from "./context";
import { withEmotionCache } from "@emotion/react";
import { useContext, useEffect } from "react";
import { ChakraProvider } from "@chakra-ui/react";
import { getUserByEmail } from "./models/user";
import { getUserToken, parseJwt } from "./utils/auth";
import BrowserOnly from "./components/BrowserOnly";
import Toast from "./components/Toast";
export const meta: MetaFunction = () => ({
  charset: "utf-8",
  title: "BV Store",
  viewport: "width=device-width,initial-scale=1",
});

export const loader: LoaderFunction = async ({ request }) => {
  const userToken = await getUserToken(request);

  const data = parseJwt(userToken || "");

  const user = await getUserByEmail(data && data.email);

  return json({ user });
};

export default function App() {
  // const { user } = useLoaderData<{
  //   user: IUser | null;
  // }>();
  // console.log("user o root : ", user);

  return (
    <Document>
      <ChakraProvider>
        <Layout>
          <Outlet />
        </Layout>
      </ChakraProvider>
    </Document>
  );
}

export let links: LinksFunction = () => {
  return [
    { rel: "preconnect", href: "https://fonts.googleapis.com" },
    { rel: "preconnect", href: "https://fonts.gstatic.com" },
    {
      rel: "stylesheet",
      href: "https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap",
    },
    {
      rel: "stylesheet",
      href: styles,
    },
  ];
};

interface DocumentProps {
  children: React.ReactNode;
}

const Document = withEmotionCache(
  ({ children }: DocumentProps, emotionCache) => {
    const serverStyleData = useContext(ServerStyleContext);
    const clientStyleData = useContext(ClientStyleContext);

    // Only executed on client
    useEffect(() => {
      // re-link sheet container
      emotionCache.sheet.container = document.head;
      // re-inject tags
      const tags = emotionCache.sheet.tags;
      emotionCache.sheet.flush();
      tags.forEach((tag) => {
        (emotionCache.sheet as any)._insertTag(tag);
      });
      // reset cache to reapply global styles
      clientStyleData?.reset();
    }, []);

    return (
      <html lang="en">
        <head>
          <Meta />
          <Links />
          {serverStyleData?.map(({ key, ids, css }) => (
            <style
              key={key}
              data-emotion={`${key} ${ids.join(" ")}`}
              dangerouslySetInnerHTML={{ __html: css }}
            />
          ))}
        </head>
        <body>
          {children}
          <BrowserOnly>
            <Toast />
          </BrowserOnly>

          <ScrollRestoration />

          <Scripts />
          <LiveReload />
        </body>
      </html>
    );
  }
);
