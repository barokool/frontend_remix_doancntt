import { useMatches } from "@remix-run/react";

export const useRouteData = (routeId: string) => {
  const matches = useMatches();
  const data = matches.find((match) => {
    if (match.id === routeId) return match;
    return null;
  });

  return data;
};
