import { useLocation } from "@remix-run/react";
import { useEffect, useLayoutEffect, useRef } from "react";

export const useKeepPosition = () => {
  const position = useRef(0);
  const location = useLocation();
  useEffect(() => {
    window.scrollTo(0, position.current);
  }, [location]);

  useLayoutEffect(() => {
    return () => {
      position.current = window.scrollY;
    };
  }, [location]);
};
