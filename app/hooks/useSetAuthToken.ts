export const useSetAuthToken = (token: string) => {
  if (token) return "Authorization";
  return null;
};
