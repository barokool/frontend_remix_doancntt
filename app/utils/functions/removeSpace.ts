export const removeSpaceString = (str: string) => {
  return str.split(" ").join("");
};
