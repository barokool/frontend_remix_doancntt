export const upperFirstLetter = (str: string) => {
  if (!str) return "";
  return str?.slice(0, 1).toUpperCase().concat(str.slice(1));
};
