export const isObjectNull = (obj: Object) => {
  if (!obj) return false;
  return Object.keys(obj).length === 0;
};
