import type { IProduct } from "~/models/products";

export const getCartProducts = (): IProduct[] => {
  return JSON.parse(localStorage.getItem("cart")!);
};
