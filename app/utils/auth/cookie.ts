export const getCookie = (cookie: string | null, name: string) => {
  if (!cookie || !name) return null;

  const findCookie = cookie
    .split?.(";")
    ?.find((item) => item?.split?.("=")?.[0]?.trim() === name);
  if (!findCookie) return null;

  return findCookie.split("=")?.[1];
};
