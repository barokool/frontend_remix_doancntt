import React, { CSSProperties } from "react";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbSeparator,
} from "@chakra-ui/react";
import RightArrow from "~/icons/RightArrow";
import { CSSObject } from "@emotion/react";
import { upperFirstLetter } from "~/utils/functions/upperFirstLetter";

interface IBreadCrumbProps {
  label: string;
  href?: string;
  css?: CSSObject;
  path?: string[];
}
const BreadCrumb: React.FC<IBreadCrumbProps> = ({
  label,
  href = "#",
  css,
  path,
}) => {
  return (
    <Breadcrumb css={css} spacing="8px" separator={<RightArrow />}>
      <BreadcrumbItem>
        <BreadcrumbLink href="/">Home</BreadcrumbLink>
      </BreadcrumbItem>

      {/* <BreadcrumbItem>
        <BreadcrumbLink isCurrentPage href={href}>
          {label}
        </BreadcrumbLink>
      </BreadcrumbItem> */}

      {path &&
        path.map((item) => {
          if (item !== "")
            return (
              <BreadcrumbItem>
                <BreadcrumbLink href={"#"}>
                  {upperFirstLetter(item)}
                </BreadcrumbLink>
              </BreadcrumbItem>
            );
        })}
    </Breadcrumb>
  );
};

export default BreadCrumb;
