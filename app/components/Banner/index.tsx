import styled from "@emotion/styled";
import React from "react";

const Banner = () => {
  return (
    <BannerSection>
      <h1 style={{ fontWeight: "700", fontSize: "36px", textAlign: "center" }}>
        Custom print on demand products
      </h1>
      <p style={{ textAlign: "center", fontSize: "18px" }}>
        Specially made for dropshipping, white label branding, merchandising and
        promoting brands
      </p>
      <WrapperBanner>
        <BannerContainer>
          <BannerContent>
            <p>Special deal for a special shirt</p>
            <button>Buy now</button>
          </BannerContent>
          <img
            src="https://i.pinimg.com/originals/4e/be/50/4ebe50e2495b17a79c31e48a0e54883f.png"
            alt="shirt-banner"
          />
        </BannerContainer>
        <BannerContainer>
          <BannerContent>
            <p>Special deal for a special shirt</p>
            <button>Buy now</button>
          </BannerContent>
          <img
            src="https://i.pinimg.com/originals/4e/be/50/4ebe50e2495b17a79c31e48a0e54883f.png"
            alt="shirt-banner"
          />
        </BannerContainer>
      </WrapperBanner>
    </BannerSection>
  );
};

export default Banner;

const BannerSection = styled.section`
  margin-top: 20px;
  display: flex;
  flex-direction: column;
  gap: 20px;
`;

const WrapperBanner = styled.div`
  display: flex;
  gap: 20px;
`;

const BannerContainer = styled.div`
  display: grid;
  width: 100%;
  grid-template-columns: 1fr 200px;
  background-color: rgba(239, 250, 250, 1);
  img {
    object-fit: cover;
  }
`;

const BannerContent = styled.div`
  padding: 40px 45px;
  display: flex;
  flex-direction: column;
  gap: 20px;
  p {
    font-size: 22px;
    font-weight: 700;
  }
  button {
    width: max-content;
    background-color: var(--primary);
    color: rgba(255, 255, 255, 1);
    padding: 8px 25px;
    font-size: 16px;
    font-weight: 600;
    border-radius: 5px;
  }
`;
