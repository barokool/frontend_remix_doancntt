import type { SerializeFrom } from "@remix-run/node";
import { Link } from "@remix-run/react";
import React from "react";
import type { StyledCompProps } from "~/design-system/stitches.config";
import { styled } from "~/design-system/stitches.config";
import type { IProduct } from "~/models/products";
interface IProductCardProps<T> {
  product: SerializeFrom<T> | T;
  wrapperStyled?: StyledCompProps<typeof ProductWrapper>;
}
const ProductCard: React.FC<IProductCardProps<IProduct>> = ({
  product,
  wrapperStyled,
}) => {
  return (
    <ProductWrapper to={`/product-detail/${product.slug}`} {...wrapperStyled}>
      <img src={product.images[0]} alt="product-alt" />
      <Title>{product.name}</Title>
      <span style={{ fontWeight: "500", fontSize: "18px" }}>
        $ {product.price.replace(/\s/g, "")}
      </span>
    </ProductWrapper>
  );
};

export default ProductCard;

const ProductWrapper = styled(Link, {
  padding: "10px",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  borderWidth: "1px",
  borderStyle: "solid",
  borderColor: "rgba(238, 238, 238, 1)",
  gap: "10px",
  width: "calc((100% - 100px) / 5)",
  cursor: "pointer",
  borderRadius: "8px",
  img: {
    display: "block",
    aspectRatio: "1",
    width: "100%",
    objectFit: "contain",
  },
  " &:hover": {
    boxShadow: "5px 6px 22px -8px rgba(0, 0, 0, 0.63)",
  },
});

const Title = styled("h3", {
  display: "-webkit-box",
  "-webkit-box-orient": "vertical",
  overflow: "hidden",
  textOverflow: "ellipsis",
  overflowWrap: "break-word",
  "-webkit-line-clamp": 2,
});
