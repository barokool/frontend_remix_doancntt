import styled from "@emotion/styled";
import React from "react";

type IBaseInputProps = {} & React.InputHTMLAttributes<HTMLInputElement>;

const BaseInput = React.forwardRef<HTMLInputElement, IBaseInputProps>(
  ({ className = "", ...props }, ref) => {
    return <input className={`${className}`} {...props} ref={ref} />;
  }
);

export default BaseInput;
