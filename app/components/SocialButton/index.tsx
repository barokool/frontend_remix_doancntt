import React from "react";
import { styled } from "~/design-system/stitches.config";
import Facebook from "~/icons/Facebook";
import Google from "~/icons/Google";
import BaseButton from "../Button";

interface ISocialButtonsProps {
  titleGoogle: string;
  titleFacebook: string;
}

const SocialButtons: React.FC<ISocialButtonsProps> = ({
  titleFacebook,
  titleGoogle,
}) => {
  return (
    <>
      <SocialButton icon={<Google />} title={titleGoogle} />
      <SocialButton icon={<Facebook />} title={titleFacebook} />
    </>
  );
};

export default SocialButtons;

const SocialButton = styled(BaseButton, {
  width: "100%",
  padding: "15px 25px",
  display: "flex",
  gap: "$x4 !important",
  marginBottom: "20px",
  borderColor: "rgba(177, 177, 177, 1)",
  fontSize: "18px",
  fontWeight: "500",
  svg: {
    width: "25px",
  },
});
