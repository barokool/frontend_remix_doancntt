import React, { CSSProperties, ReactElement } from "react";
import styled from "@emotion/styled";

interface IBaseButton extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  title?: string;
  icon?: ReactElement;
}

const BaseButton: React.FC<IBaseButton> = ({
  title = "Button",
  icon,
  ...props
}) => {
  return (
    <Container {...props}>
      {icon}
      <p>{title}</p>
    </Container>
  );
};

export default BaseButton;

const Container = styled.button`
  border: 1px solid #1e1e1e;
  padding: 5px 14px;
  border-radius: 6px;
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 5px;
`;
