import styled from "@emotion/styled";
import { Link, LinkProps } from "@remix-run/react";
import React, { FC } from "react";
import { useLocation } from "react-router";

const ActivationLink: FC<LinkProps> = function ({
  className,
  children,
  ...props
}) {
  const { pathname } = useLocation();
  console.log("pathname ", pathname);

  const to = props.to;

  const toPath =
    typeof to === "string"
      ? new URL(to, "http://localhost/").pathname
      : to?.pathname || pathname;

  const isActive = toPath === pathname;

  const initialClassName = className || "";
  const computedClassName = initialClassName + (isActive ? " active" : "");

  return (
    <Link className={computedClassName} {...props}>
      {children}
    </Link>
  );
};

export default ActivationLink;
