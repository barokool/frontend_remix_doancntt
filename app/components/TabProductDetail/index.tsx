import React from "react";
import {
  Avatar,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
} from "@chakra-ui/react";
import styled from "@emotion/styled";
const TabProductDetail = () => {
  return (
    <Tabs variant={"unstyled"}>
      <TabList>
        <Tab
          _selected={{ borderBottomColor: "black", borderBottomWidth: "2px" }}
          fontSize={24}
        >
          Details
        </Tab>
        <Tab
          _selected={{ borderBottomColor: "black", borderBottomWidth: "2px" }}
          fontSize={24}
        >
          Reviews
        </Tab>
      </TabList>

      <TabPanels>
        <TabPanel>
          <p style={{ color: "rgb(85, 85, 85)" }}>
            This ¾ sleeve raglan tee is a classic piece in modern colors made
            from 100% ring-spun cotton. This shirt is reactive-dyed which means
            its colors will be bright and vibrant for a long time. This classic
            tee will be a perfect addition to a casual look. Get it for yourself
            or sell it in your store!
          </p>
          <div style={{ marginLeft: "20px", marginTop: "10px" }}>
            <ul>
              {listDescriptions.map((item) => (
                <li key={item} style={{ color: "rgb(85, 85, 85)" }}>
                  {item}
                </li>
              ))}
            </ul>
          </div>
        </TabPanel>
        <TabPanel>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              gap: "20px",
            }}
          >
            {reviews.map((item, index) => {
              return (
                <UserComment
                  style={
                    index !== reviews.length - 1
                      ? { borderBottomWidth: "1px", borderBottomColor: "black" }
                      : {}
                  }
                  key={index}
                >
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      gap: "10px",
                    }}
                  >
                    <Avatar src={item.avatar} />
                    <p>{item.author}</p>
                  </div>
                  <p style={{ fontSize: "16px" }}>{item.comment}</p>
                </UserComment>
              );
            })}
          </div>
        </TabPanel>
      </TabPanels>
    </Tabs>
  );
};

export default TabProductDetail;

const UserComment = styled.div`
  display: flex;
  gap: 20px;
  align-items: center;
`;

const listDescriptions = [
  " All solid colors are 100% ring-spun cotton",
  "Heather Grey color is 90% cotton, 10% polyester",
  "Heather Denim color is 50% cotton, 50% polyester",
  "Fabric weight: 4.5 oz/yd² (152.6 g/m²)",
  "Fine knit jersey",
  "30 singles",
  "¾ sleeves",
  "Contrast raglan sleeve",
  "Reactive-dyed for longer-lasting color",
  "Prewashed to minimize shrinkage",
  "Tear-away label",
  "Blank product sourced from Mexico",
];

const reviews = [
  {
    author: "Gia Bao",
    comment: "This product is super cool and nice",
    stars: 5,
    avatar: "https://bit.ly/dan-abramov",
  },
  {
    author: "Viet Van",
    comment: "This product made me feel comfortable",
    stars: 4,
    avatar: "https://bit.ly/tioluwani-kolawole",
  },
  {
    author: "Gia Bao",
    comment: "Its kinda bad",
    stars: 1,
    avatar: "https://bit.ly/ryan-florence",
  },
  {
    author: "Gia Bao",
    comment: "This product is super cool and nice",
    stars: 5,
    avatar: "https://bit.ly/sage-adebayo",
  },
];
